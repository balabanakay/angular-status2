# Status Monitoring of a Web Site ( Angular RESTful Service consumption)

This project provides two pages for monitoring a web site's status. 
One for status monitoring download, One for Log interval settings. In Log interval settings the user can change the log monitoring interval dynamicaly. In this case the interval changes will affect immediately, without restarting the application.

Simply clone the project or download and extract the .zip to get started. 

To be able to use his project; it is required to download and install [WatcherRestApi project ]([https://bitbucket.org/balabanakay/watcherrestapi]).

## Concepts Covered

* To be able to use RxJS observables concepts with subscriber to reflect the changes at the data.
* The Restful service is end points are :
[posts](http://localhost:9000/api/dataservice/posts) 
[intervals](http://localhost:9000/api/dataservice/intervals)
* To be able to update the interval setting the fallowing REST Api is being used:
[single interval](http://localhost:9000/api/dataservice/interval/1)


## Software Requirements To Run Locally 

* Node.js 4.0.0 or higher
* npm 3.10.10 or higher

## Checking Installations

* Check if the node.js is intalled or from command line by using this command: node --version
* Check if the npm is installed or not from command line by using the fallowing command : npm -v

## Running the Application Locally

1. Install Node.js on your dev box

1. Install Nodemon: `npm install nodemon -g`

1. Run `npm install` to install app dependencies

1. Run `npm start` to compile the TypeScript and start the server

1. Browse to http://localhost:3000

## Screenshots


![Screen Shot 2017-03-12 at 5.23.05 PM.png](https://bitbucket.org/repo/BgooErK/images/3997930548-Screen%20Shot%202017-03-12%20at%205.23.05%20PM.png)

![Screen Shot 2017-03-12 at 5.23.15 PM.png](https://bitbucket.org/repo/BgooErK/images/3939587722-Screen%20Shot%202017-03-12%20at%205.23.15%20PM.png)

![Screen Shot 2017-03-12 at 5.23.22 PM.png](https://bitbucket.org/repo/BgooErK/images/3214204247-Screen%20Shot%202017-03-12%20at%205.23.22%20PM.png)