import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Subscription } from 'rxjs/Rx';

import { IStatus } from '../shared/interfaces';
import { DataService } from '../shared/services/data.service';

@Component({
    moduleId: module.id,
    selector: 'my-dashboard',
    templateUrl: 'dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy{

    private timerSubscription: Subscription;
    private statusSubscription: Subscription;

    statusList: IStatus[] = [];
    errorMessage: string;

    constructor(private dataService:DataService) {}

    public ngOnInit(){
       this.refreshData();
    }

    public ngOnDestroy(){
        if (this.statusSubscription){
            this.statusSubscription.unsubscribe();
        }
        if (this.timerSubscription){
            this.timerSubscription.unsubscribe();
        }
    
    }



    private refreshData(): void {
        this.statusSubscription = this.dataService.getStatusSummary()
            .subscribe((data: IStatus[]) => {
                this.statusList = data;
                this.subscribeToData();
            });
    }

    
    private subscribeToData():void {
        this.timerSubscription = Observable.timer(1000).first().subscribe( () => this.refreshData() );
    }

}