import { Component, OnInit} from '@angular/core';
import { Router} from '@angular/router';

import { Interval }         from '../shared/interval';
import { IntervalService }  from '../shared/services/interval.service';

@Component({
    moduleId: module.id,
    selector: 'my-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.css']   
})
export class AdminComponent implements OnInit {

    intervals: Interval[];
    interval: Interval;

    constructor(
        private intervalService: IntervalService,
        private router: Router
    ) { }

    ngOnInit(){
        this.getIntervals();
    }


    getIntervals(): void {
        this.intervalService.getIntervals().then(intervals => this.intervals = intervals);
    }

    change(): void {
        const intervalId = 1;
        this.router.navigate(['/detail', intervalId]);
    }



}