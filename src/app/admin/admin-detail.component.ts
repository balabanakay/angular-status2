import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap'; 

import { Interval }         from '../shared/interval';
import { IntervalService }  from '../shared/services/interval.service';

@Component({
    moduleId: module.id,
    selector: 'my-admin-detail',
    templateUrl: 'admin-detail.component.html',
    styleUrls: ['./admin-detail.component.css']   
})
export class AdminDetailComponent implements OnInit {

    interval: Interval;

    constructor(
        private intervalService: IntervalService,
        private route: ActivatedRoute,
        private location: Location
    ) { }

    ngOnInit(): void {
        this.route.params
            .switchMap((params: Params) => this.intervalService.getInterval(+params['id']))
            .subscribe(interval => this.interval = interval);
    }

    save(): void {
        this.intervalService.update(this.interval)
            .then( () => this.goBack());
    }
    goBack(): void{
        this.location.back();
    }

}