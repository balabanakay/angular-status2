export interface IStatus{
    id: number;
    severityType: string;
    severityCount: string;
}