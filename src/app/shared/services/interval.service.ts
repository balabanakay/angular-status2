import { Injectable }       from '@angular/core';
import { Http, Headers, Response  }   from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Interval }           from '../interval'; 

@Injectable()
export class IntervalService {

    private headers = new Headers({'Content-Type': 'application/json'});
    private baseUrl = 'http://localhost:9000/api/dataservice' ;

    constructor(private http: Http) { }

    getIntervals(): Promise<Interval[]> {
        return this.http.get(this.baseUrl + "/intervals")
                .map((resp: Response) => resp.json())
                .catch(this.handleError)
                .toPromise();
    }

    getInterval(id: number): Promise<Interval>{
        const singleUrl = this.baseUrl + "/interval"
        const url = `${singleUrl}/${id}`;
        return this.http.get(url)
                .map((resp: Response) => resp.json())
                .catch(this.handleError)
                .toPromise();
    }

    update(interval: Interval): Promise<Interval> {
        const singleUrl = this.baseUrl + "/interval"
        const url = `${singleUrl}/${interval.id}`;
          return this.http
                    .put(url, JSON.stringify(interval), {headers: this.headers} )
                    .map((resp: Response) => resp.json())
                    .catch(this.handleError)
                    .toPromise();

    }


    private handleError(error:any){
        console.error(error);
        return Promise.reject(error.message || error);

    }

}