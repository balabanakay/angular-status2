import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


import { IStatus } from '../interfaces';

@Injectable()
export class DataService {

    //http://localhost:9000/api/dataservice/status
    private url: string = 'http://localhost:9000/api/dataservice';
    constructor( private http: Http) { }

    getStatusSummary() : Observable<IStatus[]>{
        return this.http.get (this.url + "/posts")
                .map((resp: Response) => resp.json())
                   .catch(this.handleError);
    }

    handleError(error: any){
        console.error(error);
        return Observable.throw(error.json().error || 'Service error in Data Service');
    }

}