import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule} from '@angular/http';

import { AppComponent }  from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminComponent } from './admin/admin.component';
import { AdminDetailComponent } from './admin/admin-detail.component';
import { DataService } from './shared/services/data.service';

import { IntervalService } from './shared/services/interval.service';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  imports:      [ 
    BrowserModule,
    FormsModule ,
    ReactiveFormsModule, 
    HttpModule,
    AppRoutingModule
  ],
  declarations: [ 
    AppComponent,
    DashboardComponent,
    AdminComponent,
    AdminDetailComponent
  ],
  providers: [DataService, IntervalService ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
